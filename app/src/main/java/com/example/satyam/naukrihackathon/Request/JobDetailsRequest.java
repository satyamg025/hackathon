package com.example.satyam.naukrihackathon.Request;

import com.example.satyam.naukrihackathon.Models.JobDetailsPOJO;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by satyam on 6/5/17.
 */

public interface JobDetailsRequest {
    @GET("jobs")
    Call<List<JobDetailsPOJO>> requestResponse(@Query("limit") String limit);
}
