package com.example.satyam.naukrihackathon.Adapter;

import android.content.Context;
import android.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.satyam.naukrihackathon.Activity.MainActivity;
import com.example.satyam.naukrihackathon.Fragment.fragment_details;
import com.example.satyam.naukrihackathon.Models.JobDetailsPOJO;
import com.example.satyam.naukrihackathon.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by satyam on 1/15/17.
 */
public class desc_adapter extends RecyclerView.Adapter<desc_adapter.ViewHolder> {

    List<JobDetailsPOJO> jobDetails=new ArrayList<JobDetailsPOJO>();
    Context context;
    FragmentManager manager;


    public desc_adapter(List<JobDetailsPOJO> jobDetailsPOJO, MainActivity context, FragmentManager supportFragmentManager) {
        this.jobDetails=jobDetailsPOJO;
        this.context=context;
        this.manager=supportFragmentManager;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_job, parent, false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.company_name.setText(jobDetails.get(position).getCompanyName());
            holder.type.setText(jobDetails.get(position).getType());
            holder.location.setText(jobDetails.get(position).getLocation());
    }


    @Override
    public int getItemCount() {
        return jobDetails.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView company_name,type,location;
        public ViewHolder(View itemView) {
            super(itemView);

            company_name=(TextView)itemView.findViewById(R.id.company_name);
            type=(TextView)itemView.findViewById(R.id.type);
            location=(TextView)itemView.findViewById(R.id.location);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragment_details fragment_details= com.example.satyam.naukrihackathon.Fragment.fragment_details.instance(jobDetails.get(getAdapterPosition()));
                    fragment_details.show(manager,"Details");
                }
            });
        }

    }
}

