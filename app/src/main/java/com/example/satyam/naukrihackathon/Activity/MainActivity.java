package com.example.satyam.naukrihackathon.Activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.satyam.naukrihackathon.Adapter.desc_adapter;
import com.example.satyam.naukrihackathon.Models.JobDetailsPOJO;
import com.example.satyam.naukrihackathon.R;
import com.example.satyam.naukrihackathon.Request.JobDetailsRequest;
import com.example.satyam.naukrihackathon.Service.ServiceGenerator;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    RecyclerView mrecyclerView;
    LinearLayoutManager mlinearLayoutManager;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        JobDetailsRequest jobDetailsRequest= ServiceGenerator.createService(JobDetailsRequest.class);
        Call<List<JobDetailsPOJO>> call=jobDetailsRequest.requestResponse("100");
        call.enqueue(new Callback<List<JobDetailsPOJO>>() {
            @Override
            public void onResponse(Call<List<JobDetailsPOJO>> call, Response<List<JobDetailsPOJO>> response) {

                if(response.code()==200) {
                    List<JobDetailsPOJO> jobDetailsPOJO = new ArrayList<JobDetailsPOJO>();
                    jobDetailsPOJO = response.body();

                    progressDialog.dismiss();
                    mrecyclerView = (RecyclerView) findViewById(R.id.recycler);
                    assert mrecyclerView != null;
                    mrecyclerView.setHasFixedSize(true);
                    mlinearLayoutManager = new LinearLayoutManager(getApplicationContext());
                    mrecyclerView.setLayoutManager(mlinearLayoutManager);
                    RecyclerView.Adapter mAdapter;
                    mAdapter = new desc_adapter(jobDetailsPOJO, MainActivity.this,getFragmentManager());
                    mrecyclerView.setAdapter(mAdapter);
                }
            }

            @Override
            public void onFailure(Call<List<JobDetailsPOJO>> call, Throwable t) {

            }
        });

    }
}
