package com.example.satyam.naukrihackathon.Fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.satyam.naukrihackathon.Models.JobDetailsPOJO;
import com.example.satyam.naukrihackathon.R;
import com.google.gson.Gson;

/**
 * Created by satyam on 6/5/17.
 */

public class fragment_details extends DialogFragment {
    View view;
    TextView comp,type,sal,loc,add,ug,pg,desc,email;
    static Gson gson=new Gson();
    JobDetailsPOJO details;
    Button btn;

    public static fragment_details instance(JobDetailsPOJO jobDetailsPOJO){

        fragment_details fragment_details=new fragment_details();
        Bundle bundle=new Bundle();
        bundle.putString("details",gson.toJson(jobDetailsPOJO));
        fragment_details.setArguments(bundle);
        return  fragment_details;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        view = inflater.inflate(R.layout.fragment_details, null);
        builder.setView(view);
        comp=(TextView)view.findViewById(R.id.company_name);
        type=(TextView)view.findViewById(R.id.type);
        sal=(TextView)view.findViewById(R.id.salary);
        loc=(TextView)view.findViewById(R.id.location);
        add=(TextView)view.findViewById(R.id.address);
        ug=(TextView)view.findViewById(R.id.ug);
        pg=(TextView)view.findViewById(R.id.pg);
        desc=(TextView)view.findViewById(R.id.desc);
        email=(TextView)view.findViewById(R.id.email);
        btn=(Button)view.findViewById(R.id.apply);

        details=gson.fromJson(getArguments().getString("details"),JobDetailsPOJO.class);
        comp.setText(details.getCompanyName());
        type.setText(details.getType());
        sal.setText(details.getSalary());
        loc.setText(details.getLocation());
        add.setText(details.getCompanyAddress());
        ug.setText(details.getUgRequirement());
        pg.setText(details.getPgRequirement());
        email.setText(details.getEmail());
        desc.setText(details.getDescription());

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),details.getJobId(),Toast.LENGTH_LONG).show();
            }
        });
        return builder.create();
    }

}