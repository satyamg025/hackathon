package com.example.satyam.naukrihackathon.Models;

/**
 * Created by satyam on 6/5/17.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobDetailsPOJO {


        @SerializedName("jobId")
        @Expose
        private String jobId;
        @SerializedName("0")
        @Expose
        private String _0;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("1")
        @Expose
        private String _1;
        @SerializedName("companyName")
        @Expose
        private String companyName;
        @SerializedName("2")
        @Expose
        private String _2;
        @SerializedName("salary")
        @Expose
        private String salary;
        @SerializedName("3")
        @Expose
        private String _3;
        @SerializedName("companyAddress")
        @Expose
        private String companyAddress;
        @SerializedName("4")
        @Expose
        private String _4;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("5")
        @Expose
        private String _5;
        @SerializedName("location")
        @Expose
        private String location;
        @SerializedName("6")
        @Expose
        private String _6;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("7")
        @Expose
        private String _7;
        @SerializedName("ugRequirement")
        @Expose
        private String ugRequirement;
        @SerializedName("8")
        @Expose
        private String _8;
        @SerializedName("pgRequirement")
        @Expose
        private String pgRequirement;
        @SerializedName("9")
        @Expose
        private String _9;

        public String getJobId() {
            return jobId;
        }

        public void setJobId(String jobId) {
            this.jobId = jobId;
        }

        public String get0() {
            return _0;
        }

        public void set0(String _0) {
            this._0 = _0;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String get1() {
            return _1;
        }

        public void set1(String _1) {
            this._1 = _1;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String get2() {
            return _2;
        }

        public void set2(String _2) {
            this._2 = _2;
        }

        public String getSalary() {
            return salary;
        }

        public void setSalary(String salary) {
            this.salary = salary;
        }

        public String get3() {
            return _3;
        }

        public void set3(String _3) {
            this._3 = _3;
        }

        public String getCompanyAddress() {
            return companyAddress;
        }

        public void setCompanyAddress(String companyAddress) {
            this.companyAddress = companyAddress;
        }

        public String get4() {
            return _4;
        }

        public void set4(String _4) {
            this._4 = _4;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String get5() {
            return _5;
        }

        public void set5(String _5) {
            this._5 = _5;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String get6() {
            return _6;
        }

        public void set6(String _6) {
            this._6 = _6;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String get7() {
            return _7;
        }

        public void set7(String _7) {
            this._7 = _7;
        }

        public String getUgRequirement() {
            return ugRequirement;
        }

        public void setUgRequirement(String ugRequirement) {
            this.ugRequirement = ugRequirement;
        }

        public String get8() {
            return _8;
        }

        public void set8(String _8) {
            this._8 = _8;
        }

        public String getPgRequirement() {
            return pgRequirement;
        }

        public void setPgRequirement(String pgRequirement) {
            this.pgRequirement = pgRequirement;
        }

        public String get9() {
            return _9;
        }

        public void set9(String _9) {
            this._9 = _9;
        }
}
